package id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.skill;

public class ThousandYearsOfPain implements Skill {

    @Override
    public String getName() {
        return "1000 Years of Pain";
    }

    @Override
    public String getDescription() {
        return "The ultimate skill that gives great damage to enemies";
    }
}
