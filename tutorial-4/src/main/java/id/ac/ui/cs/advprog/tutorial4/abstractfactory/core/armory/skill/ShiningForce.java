package id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.skill;

public class ShiningForce implements Skill {

    @Override
    public String getName() {
        return "Shining Force";
    }

    @Override
    public String getDescription() {
        return "The skill that can turn enemies to ashes";
    }
}
