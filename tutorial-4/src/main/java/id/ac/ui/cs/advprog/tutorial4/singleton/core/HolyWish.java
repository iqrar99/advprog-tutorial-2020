package id.ac.ui.cs.advprog.tutorial4.singleton.core;

public class HolyWish {

    private String wish;
    private static HolyWish instance;

    // TODO complete me with any Singleton approach
    private HolyWish(){}

    public String getWish() {
        return wish;
    }

    public void setWish(String wish) {
        this.wish = wish;
    }

    @Override
    public String toString() {
        return wish;
    }

	public static HolyWish getInstance() {
        if (instance == null){
            instance = new HolyWish();
        }
        return instance;
	}

}
