package id.ac.ui.cs.advprog.tutorial4.singleton;

import java.lang.reflect.Constructor;
import java.lang.reflect.Modifier;
import java.util.Arrays;
import java.util.List;

import id.ac.ui.cs.advprog.tutorial4.singleton.core.HolyWish;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class HolyWishTest {

    private Class<?> holyWishClass;
    private HolyWish holyWish;

    @BeforeEach
    public void setUp() throws Exception {
        holyWishClass = Class.forName(HolyWish.class.getName());
        holyWish = HolyWish.getInstance();
    }

    @Test
    public void testNoPublicConstructors() {
        List<Constructor> constructors = Arrays.asList(holyWishClass.getDeclaredConstructors());

        boolean check = constructors.stream()
                .anyMatch(c -> !Modifier.isPrivate(c.getModifiers()));

        assertFalse(check);
    }

    @Test
    public void testGetInstanceShouldReturnSingletonInstance() {
        HolyWish aWish = HolyWish.getInstance();
        assertEquals(aWish, holyWish);
    }

    @Test
    public void testWish(){
        holyWish.setWish("I want to get an internship");
        assertEquals("I want to get an internship", holyWish.getWish());
    }

    @Test
    public void testWishExist(){
        HolyWish holyWish2 = HolyWish.getInstance();
        
        // holywish2 should have a same wish with holywish due singelton
        holyWish.setWish("I want to have a gf");
        assertNotNull(holyWish2);
        assertEquals("I want to have a gf", holyWish2.getWish());

    }

}
