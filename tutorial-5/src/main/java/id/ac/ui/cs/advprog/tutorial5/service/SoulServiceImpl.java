package id.ac.ui.cs.advprog.tutorial5.service;

import id.ac.ui.cs.advprog.tutorial5.entity.Soul;
import id.ac.ui.cs.advprog.tutorial5.repository.SoulRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class SoulServiceImpl implements SoulService{

    @Autowired
    private SoulRepository repository;
    public SoulServiceImpl(SoulRepository repository){
        this.repository = repository;
    }

    @Override
    public List<Soul> findAll() {
        return repository.findAll();
    }

    @Override
    public Optional<Soul> findSoul(Long id) {
        return repository.findById(id);
    }

    @Override
    public void erase(Long id) {
        repository.deleteById(id);
    }

    @Override
    public Soul rewrite(Soul soul) {
        return repository.save(soul);
    }

    @Override
    public Soul register(Soul soul) {
        return repository.save(soul);
    }

}
