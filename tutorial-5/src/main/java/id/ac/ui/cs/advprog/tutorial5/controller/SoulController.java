package id.ac.ui.cs.advprog.tutorial5.controller;


import id.ac.ui.cs.advprog.tutorial5.entity.Soul;
import id.ac.ui.cs.advprog.tutorial5.service.SoulService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(path = "/soul")
public class SoulController {

    @Autowired
    private final SoulService soulService;
    public SoulController(SoulService soulService) {
        this.soulService = soulService;
    }

    @GetMapping
    public ResponseEntity<List<Soul>> findAll(){
        return new ResponseEntity<List<Soul>>(soulService.findAll(), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity create(@RequestBody Soul soul){
        Optional<Soul> newSoul = soulService.findSoul(soul.getId());
        if (newSoul.isPresent()){
            return new ResponseEntity("Can't be duplicated!", HttpStatus.BAD_REQUEST);
        }
        Soul createdSoul = soulService.register(soul);
        return new ResponseEntity("OK!", HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Optional<Soul>> findById(@PathVariable Long id){
        Optional<Soul> soul = soulService.findSoul(id);
        if (!soul.isPresent()){
            return new ResponseEntity<Optional<Soul>>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<Optional<Soul>>(soul, HttpStatus.OK);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Soul> update(@PathVariable Long id, @RequestBody Soul soul){
        Optional<Soul> newSoul = soulService.findSoul(soul.getId());
        if (id != soul.getId() || !newSoul.isPresent()){
            return new ResponseEntity<Soul>(HttpStatus.NOT_FOUND);
        }

        Soul modifiedSoul = newSoul.get();
        modifiedSoul.setName(soul.getName());
        modifiedSoul.setAge(soul.getAge());
        modifiedSoul.setOccupation(soul.getOccupation());
        modifiedSoul.setGender(soul.getGender());

        return new ResponseEntity<Soul>(soulService.rewrite(modifiedSoul), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity delete(@PathVariable Long id){
        Optional<Soul> soul = soulService.findSoul(id);
        if (!soul.isPresent()){
            return new ResponseEntity<Soul>(HttpStatus.NOT_FOUND);
        }
        soulService.erase(id);
        return new ResponseEntity("OK!", HttpStatus.OK);
    }
}
