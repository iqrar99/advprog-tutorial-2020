package id.ac.ui.cs.advprog.tutorial5.entity;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class SoulTest {
    private Soul soul;

    @BeforeEach
    public void setUp(){
        soul = new Soul();
        soul = new Soul(1, "Iqrar", 20, "M", "Student");
    }

    @Test
    public void testGetId(){
        assertEquals(1, soul.getId());
    }

    @Test
    public void testGetName(){
        assertEquals("Iqrar", soul.getName());
    }

    @Test
    public void testSetName(){
        soul.setName("Asyifa");
        assertEquals("Asyifa", soul.getName());
    }

    @Test
    public void testGetAge(){
        assertEquals(20, soul.getAge());
    }

    @Test
    public void testSetAge(){
        soul.setAge(14);
        assertEquals(14, soul.getAge());
    }

    @Test
    public void testGetGender(){
        assertEquals("M", soul.getGender());
    }

    @Test
    public void testSetGender(){
        soul.setGender("F");
        assertEquals("F", soul.getGender());
    }

    @Test
    public void testGetOccupation(){
        assertEquals("Student", soul.getOccupation());
    }

    @Test
    public void testSetOccupation(){
        soul.setOccupation("Villager");
        assertEquals("Villager", soul.getOccupation());
    }
}
