package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AgileAdventurer extends Adventurer {

        public AgileAdventurer(){
                setDefenseBehavior(new DefendWithBarrier());
                setAttackBehavior(new AttackWithGun());
        }

        @Override
        public String getAlias() {
                return "Agile";
        }
}
